# Ubuntu multiarch pipeline

This repository was forked from [Debian multiarch pipeline](https://gitlab.com/vicamo/debian-pipeline). Please check documentation there as this repository should only differ in suite names.

## Contribution

You should always consider upstream your feature to Debian multiarch pipeline whenever appropriate as this project will merge its HEAD. Anyway, contribution is still welcome and appriciated.
